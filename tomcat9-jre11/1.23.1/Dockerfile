FROM alpine:3.17 as builder

ARG GWC_VERSION=1.23.1

RUN set -ex \
    # apk --update add openssl wget unzip \
    # Download and install GWC
    && wget --no-check-certificate --quiet -O /tmp/gwc.zip -c https://sourceforge.net/projects/geowebcache/files/geowebcache/${GWC_VERSION}/geowebcache-${GWC_VERSION}-war.zip/download \
    && unzip /tmp/gwc.zip -d /tmp/gwc \
    && mkdir -p /tmp/webapps/gwc \
    && unzip /tmp/gwc/geowebcache.war -d /tmp/webapps/gwc

FROM tomcat:9-jre11 as prod

# Metadata
LABEL \
    version="1.0" \
    maintainer="SmartGeoSystem" \
    description="GeoWebCache"

ENV \
    # Common environment specific
    TZ=Europe/Moscow \
    GIT_DIR=/tmp \
    DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true \
    # App specific
    JAVA_OPTS="-server -Xmx1024M" \
    GWC_USER=geowebcache \
    GWC_PASS=secured

COPY --from=builder /tmp/webapps/gwc $CATALINA_HOME/webapps/gwc

COPY vmts-qgis-server.crt /opt/vmts-qgis-server.crt
RUN  keytool -importcert -noprompt -storepass changeit -keystore $JAVA_HOME/lib/security/cacerts -file /opt/vmts-qgis-server.crt

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["catalina.sh", "run"]

# special volumes
VOLUME /usr/local/tomcat/logs
VOLUME /usr/local/tomcat/work/Catalina
VOLUME /usr/local/tomcat/conf/Catalina
VOLUME /usr/local/tomcat/temp/gwc
VOLUME /tmp
# data volume
VOLUME /usr/local/tomcat/temp/geowebcache

EXPOSE 8080
